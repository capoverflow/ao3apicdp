package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/cdproto/runtime"
	"github.com/chromedp/cdproto/storage"
	"github.com/chromedp/chromedp"
	"gitlab.com/capoverflow/ao3apicdp/internals/models"
	"gitlab.com/capoverflow/ao3apicdp/internals/utils"
)

func DoLogin(login models.Login, ctx context.Context) {
	var url string
	authFile := fmt.Sprintf("%s.json", login.Username)
	log.Println(authFile)

	// Determine the URL scheme based on the Login struct
	if len(login.Scheme) >= 0 {
		url = fmt.Sprintf("https://%s", login.Addr)
	} else {
		url = fmt.Sprintf("%s://%s", login.Scheme, login.Addr)
	}

	var CookiesOutput []*network.Cookie

	err := utils.CheckJSONFile(authFile)
	if err == nil {
		cookiesJSON, err := os.ReadFile(authFile)
		if err != nil {
			log.Printf("Error reading  %s: %s\n", authFile, err)
		}
		var cookies []*network.Cookie

		if err := json.Unmarshal(cookiesJSON, &cookies); err != nil {
			log.Printf("Error unmarshalling %s: %s\n", authFile, err)

		} else {
			DoCookies(cookies, ctx)
			log.Println("else")
			return

		}
	}

	log.Println("Starting login process")

	// Perform the login process using chromedp
	if err := chromedp.Run(ctx,
		chromedp.Navigate(fmt.Sprintf("%s/users/login", url)),
		chromedp.ActionFunc(
			func(ctx context.Context) error {
				// Set a local storage value
				_, exp, err := runtime.Evaluate(`window.localStorage.setItem('accepted_tos', '20180523')`).Do(ctx)
				if err != nil {
					return err
				}
				if exp != nil {
					return exp
				}

				return nil
			}),
		chromedp.Navigate(fmt.Sprintf("%s/users/login", url)),

		chromedp.WaitVisible(`user_login`, chromedp.ByID),

		chromedp.SetValue(`user_login`, login.Username, chromedp.ByID),

		chromedp.SetValue(`user_password`, login.Password, chromedp.ByID),

		chromedp.ActionFunc(func(ctx context.Context) error {
			log.Println(login.RememberMe)
			if login.RememberMe {
				chromedp.Click("user_remember_me", chromedp.ByID).Do(ctx)
			}
			return nil
		}),

		chromedp.Click(`document.querySelector("#new_user > dl > dd.submit.actions > input")`, chromedp.ByJSPath),

		chromedp.Sleep(5*time.Second),

		chromedp.ActionFunc(func(ctx context.Context) error {
			var err error
			cookies, err := storage.GetCookies().Do(ctx)
			if err != nil {
				return err
			}

			for i, cookie := range cookies {
				log.Printf("chrome cookie %d: %+v", i, cookie)
				CookiesOutput = append(CookiesOutput, cookie)
			}

			return nil
		}),
	); err != nil {
		log.Println("Error during login process:", err)
	}

	// Save the cookies to auth.json
	cookiesJSON, err := json.MarshalIndent(CookiesOutput, "", "  ")
	if err != nil {
		log.Fatal("Error marshalling cookies:", err)
	}

	err = os.WriteFile(authFile, cookiesJSON, 0644)
	if err != nil {
		log.Fatalf("Error writing %s: %s\n", authFile, err)
	}
}

func DoCookies(cookies []*network.Cookie, ctx context.Context) {
	if err := chromedp.Run(ctx,

		chromedp.ActionFunc(func(ctx context.Context) error {
			var cookieParams []*network.CookieParam
			// log.Println(len(cookies))
			for _, cookie := range cookies {

				if cookie.Expires <= 0 {
					// log.Println(" no expire ")
					cookieParams = append(cookieParams, &network.CookieParam{
						Name:         cookie.Name,
						Value:        cookie.Value,
						Domain:       cookie.Domain,
						Path:         cookie.Path,
						Secure:       cookie.Secure,
						HTTPOnly:     cookie.HTTPOnly,
						SameSite:     cookie.SameSite,
						Priority:     cookie.Priority,
						SameParty:    cookie.SameParty,
						SourceScheme: cookie.SourceScheme,
						SourcePort:   cookie.SourcePort,
						PartitionKey: cookie.PartitionKey,
					})
				} else {
					t := time.Unix(int64(cookie.Expires), 0)
					// log.Println(i, cookie.Expires, t)
					expr := cdp.TimeSinceEpoch(t)

					cookieParams = append(cookieParams, &network.CookieParam{
						Name:         cookie.Name,
						Value:        cookie.Value,
						Domain:       cookie.Domain,
						Path:         cookie.Path,
						Secure:       cookie.Secure,
						HTTPOnly:     cookie.HTTPOnly,
						SameSite:     cookie.SameSite,
						Expires:      &expr,
						Priority:     cookie.Priority,
						SameParty:    cookie.SameParty,
						SourceScheme: cookie.SourceScheme,
						SourcePort:   cookie.SourcePort,
						PartitionKey: cookie.PartitionKey,
					})

				}
				storage.SetCookies(cookieParams).Do(ctx)

			}
			return nil

		}),
	); err != nil {
		log.Println(err)
	}

}
