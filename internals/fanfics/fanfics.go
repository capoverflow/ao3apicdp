package fanfics

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
	"gitlab.com/capoverflow/ao3apicdp/internals/models"
)

var (
	layoutISO = "2006-01-02"
)

func GetFanficV2(Params models.FanficParams, ctx context.Context) (fanfic models.Work) {

	var data string

	fanfic.WorkID = Params.WorkID

	fanfic.URL = fmt.Sprintf("%s://%s/works/%s", Params.Scheme, Params.Addr, Params.WorkID)

	url := fmt.Sprintf("%s://%s/works/%s?view_full_work=true&view_adult=true", Params.Scheme, Params.Addr, Params.WorkID)

	if err := chromedp.Run(ctx,
		chromedp.Sleep(1*time.Second),

		// chromedp.Navigate(),
		//

		chromedp.Navigate(url),

		// chromedp.ActionFunc(
		// 	func(ctx context.Context) error {
		// 		_, exp, err := runtime.Evaluate(`return('accepted_tos', '20180523')`).Do(ctx)
		// 		if err != nil {
		// 			return err
		// 		}
		// 		if exp != nil {
		// 			return exp
		// 		}
		// 		return nil
		// 	}),

		chromedp.Navigate(url),

		// chromedp.WaitVisible(`#MainHeader_PageTitle`, chromedp.ByQuery),

		chromedp.OuterHTML("html", &data, chromedp.ByQuery),
	); err != nil {
		log.Println(err)
	}

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(data))
	if err != nil {
		log.Println(err)
	}

	doc.Find("div.chapter.preface.group > h3.title").Each(func(i int, el *goquery.Selection) {
		var splitStr []string
		val, exist := el.Find("a").Attr("href")
		if exist {
			splitStr = strings.Split(val, "/")
		}
		// log.Println()

		chapter := models.Chapter{
			// Date:      time.Time{},
			Name:      strings.TrimSpace(el.Text()),
			ChapterID: splitStr[len(splitStr)-1],
		}
		fanfic.Chapters = append(fanfic.Chapters, chapter)
	})

	doc.Find("dl.stats").Each(func(i int, s *goquery.Selection) {
		datestr := s.Find("dd.published").Text()
		layoutISO := "2006-01-02"
		fanfic.DatePublished, err = time.Parse(layoutISO, datestr)
		if err != nil {
			log.Println(err)
		}

		Updated := s.Find("dd.status").Text()
		if len(Updated) != 0 {
			fanfic.Updated, err = time.Parse(layoutISO, Updated)
			if err != nil {
				log.Println("error with time", err)
			}
		}
	})

	doc.Find("dl.stats > dd").Each(func(i int, el *goquery.Selection) {
		// log.Println(el.Text())
		n := el.Get(0) // Retrieves the internal *html.Node
		for _, a := range n.Attr {
			// log.Println(a.Val)
			switch a.Val {
			case "language":
				fanfic.Language = el.Text()
			case "words":
				fanfic.Words = el.Text()
			case "chapters":
				fanfic.NBChapters = el.Text()
			case "comments":
				fanfic.Kudos = el.Text()

			case "kudos":
				fanfic.Kudos = el.Text()

			case "bookmarks":
				fanfic.Kudos = el.Text()

			case "hits":
				fanfic.Hits = el.Text()
			}
		}

	})

	fanfic.Title = strings.TrimSpace(doc.Find("h2.title.heading").Text())

	// text := doc.Find("h3.byline.heading").Text()
	// // fanfic.Author = strings.TrimSpace(text)
	// // log.Println(text)
	// t := strings.Split(text, ",")

	doc.Find("h3.byline.heading").Each(func(i int, s *goquery.Selection) {
		s.Find("a").Each(func(i int, el *goquery.Selection) {
			fanfic.Author = append(fanfic.Author, strings.TrimSpace(el.Text()))
		})
	})

	doc.Find("div.summary.module").Each(func(i int, s *goquery.Selection) {
		s.Find("p").Each(func(i int, el *goquery.Selection) {
			fanfic.Summary = append(fanfic.Summary, el.Text())
		})
	})

	doc.Find("dd.fandom.tags").Each(func(i int, s *goquery.Selection) {
		s.Find("a.tag").Each(func(i int, el *goquery.Selection) {
			fanfic.Fandom = append(fanfic.Fandom, el.Text())
		})
	})

	doc.Find("dd.relationship.tags").Each(func(i int, s *goquery.Selection) {
		s.Find("a.tag").Each(func(i int, el *goquery.Selection) {
			fanfic.Relationship = append(fanfic.Relationship, el.Text())
		})
	})

	doc.Find("dd.freeform.tags").Each(func(i int, s *goquery.Selection) {
		s.Find("a.tag").Each(func(i int, el *goquery.Selection) {
			if len(el.Text()) != 0 {
				fanfic.AlternativeTags = append(fanfic.AlternativeTags, el.Text())
			}
		})
	})

	doc.Find("li.download").Each(func(_ int, s *goquery.Selection) {
		// log.Println(s.Find("a").Attr("href"))
		s.Find("a").Each(func(_ int, s *goquery.Selection) {
			href, ok := s.Attr("href")
			if ok {
				_, ok := s.Attr("class")
				if !ok {
					for p := s.Parent(); p.Size() > 0 && !ok; p = p.Parent() {
						_, ok = p.Attr("class")
					}
				}
				// log.Printf("Link #%d:\nhref: %s\ntext: %s\nclass: %s\n\n", i, href, s.Text(), classes)
				if !strings.Contains(href, "#") {
					fanfic.Downloads = append(fanfic.Downloads, fmt.Sprintf("%s://download.%s%s", Params.Scheme, Params.Addr, href))
				}
			}
		})

	})

	return fanfic
}

func GetFanfic(Params models.FanficParams, ctx context.Context) (fanfic models.Work) {

	var data string

	fanfic.WorkID = Params.WorkID

	fanfic.URL = fmt.Sprintf("%s://%s/works/%s", Params.Scheme, Params.Addr, Params.WorkID)

	if err := chromedp.Run(ctx,
		chromedp.Sleep(1*time.Second),

		chromedp.Navigate(fmt.Sprintf("%s://%s/works/%s/navigate?view_adult=true", Params.Scheme, Params.Addr, Params.WorkID)),
		//

		// chromedp.WaitVisible(`#MainHeader_PageTitle`, chromedp.ByQuery),
		chromedp.Sleep(time.Second*5),

		chromedp.OuterHTML("html", &data, chromedp.ByQuery),
	); err != nil {
		log.Println(err)
	}

	log.Println(data)

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(data))
	if err != nil {
		log.Println(err)
	}
	doc.Find(".chapter > li").Each(func(i int, s *goquery.Selection) {
		var chap models.Chapter

		chap.Name = s.Find("a").Text()

		if val, ok := s.Find("a").Attr("href"); !ok {
			log.Println("no link")
		} else {
			href := strings.Split(val, "/")
			chap.ChapterID = href[len(href)-1]

		}
		datestr := s.Find("span").Text()
		datestr = strings.Trim(datestr, "(")
		datestr = strings.Trim(datestr, ")")
		t, err := time.Parse(layoutISO, datestr)
		if err != nil {
			log.Println(err)
		}
		chap.Date = t

		// chaps = append(chaps, chap)
		fanfic.Chapters = append(fanfic.Chapters, chap)
	})

	if err := chromedp.Run(ctx,
		chromedp.Sleep(1*time.Second),

		chromedp.Navigate(fmt.Sprintf("%s://%s/works/%s?view_adult=true", Params.Scheme, Params.Addr, Params.WorkID)),
		//

		// chromedp.WaitVisible(`#MainHeader_PageTitle`, chromedp.ByQuery),

		chromedp.OuterHTML("html", &data, chromedp.ByQuery),
	); err != nil {
		log.Println(err)
	}

	doc, err = goquery.NewDocumentFromReader(strings.NewReader(data))
	if err != nil {
		log.Println(err)
	}

	doc.Find("dl.stats").Each(func(i int, s *goquery.Selection) {
		datestr := s.Find("dd.published").Text()
		layoutISO := "2006-01-02"
		fanfic.DatePublished, err = time.Parse(layoutISO, datestr)
		if err != nil {
			log.Println(err)
		}

		Updated := s.Find("dd.status").Text()
		if len(Updated) != 0 {
			fanfic.Updated, err = time.Parse(layoutISO, Updated)
			if err != nil {
				log.Println("error with time", err)
			}
		}
		fanfic.Words = s.Find("dd.words").Text()

		fanfic.NBChapters = s.Find("dd.chapters").Text()

		fanfic.Comments = s.Find("dd.comments").Text()
		if err != nil {
			log.Println(err)
		}
		fanfic.Kudos = s.Find("dd.kudos").Text()
		if err != nil {
			log.Println(err)
		}
		fanfic.Bookmarks = s.Find("dd.bookmarks").Text()
		if err != nil {
			log.Println(err)
		}
		fanfic.Hits = s.Find("dd.hits").Text()
	})

	fanfic.Title = strings.TrimSpace(doc.Find("h2.title.heading").Text())

	// text := doc.Find("h3.byline.heading").Text()
	// // fanfic.Author = strings.TrimSpace(text)
	// // log.Println(text)
	// t := strings.Split(text, ",")

	doc.Find("h3.byline.heading").Each(func(i int, s *goquery.Selection) {
		s.Find("a").Each(func(i int, el *goquery.Selection) {
			fanfic.Author = append(fanfic.Author, strings.TrimSpace(el.Text()))
		})
	})

	doc.Find("div.summary.module").Each(func(i int, s *goquery.Selection) {
		s.Find("p").Each(func(i int, el *goquery.Selection) {
			fanfic.Summary = append(fanfic.Summary, el.Text())
		})
	})

	doc.Find("dd.fandom.tags").Each(func(i int, s *goquery.Selection) {
		s.Find("a.tag").Each(func(i int, el *goquery.Selection) {
			fanfic.Fandom = append(fanfic.Fandom, el.Text())
		})
	})

	doc.Find("dd.relationship.tags").Each(func(i int, s *goquery.Selection) {
		s.Find("a.tag").Each(func(i int, el *goquery.Selection) {
			fanfic.Relationship = append(fanfic.Relationship, el.Text())
		})
	})

	doc.Find("dd.freeform.tags").Each(func(i int, s *goquery.Selection) {
		s.Find("a.tag").Each(func(i int, el *goquery.Selection) {
			if len(el.Text()) != 0 {
				fanfic.AlternativeTags = append(fanfic.AlternativeTags, el.Text())
			}
		})
	})

	doc.Find("li.download").Each(func(_ int, s *goquery.Selection) {
		// log.Println(s.Find("a").Attr("href"))
		s.Find("a").Each(func(_ int, s *goquery.Selection) {
			href, ok := s.Attr("href")
			if ok {
				_, ok := s.Attr("class")
				if !ok {
					for p := s.Parent(); p.Size() > 0 && !ok; p = p.Parent() {
						_, ok = p.Attr("class")
					}
				}
				// log.Printf("Link #%d:\nhref: %s\ntext: %s\nclass: %s\n\n", i, href, s.Text(), classes)
				if !strings.Contains(href, "#") {
					fanfic.Downloads = append(fanfic.Downloads, fmt.Sprintf("%s://download.archiveofourown.org%s", Params.Scheme, href))
				}
			}
		})

	})

	// log.Println(fanfic)
	return fanfic

}
