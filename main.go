package ao3apicdp

import (
	"log"
	"os"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.SetPrefix("ao3apicdp: ")
	log.SetOutput(os.Stderr)
}
