package user

import (
	"context"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
	"gitlab.com/capoverflow/ao3apicdp/internals/models"
	"gitlab.com/capoverflow/ao3apicdp/internals/utils"
)

func GetHistory(Name string, ctx context.Context) (History []models.History) {

	var Body string

	log.Printf("Getting user history for %s\n", Name)

	url := fmt.Sprintf("https://archiveofourown.org/users/%s/readings", Name)
	if err := chromedp.Run(ctx,
		chromedp.Sleep(1*time.Second),

		chromedp.Navigate(url),

		chromedp.OuterHTML(`html > body`, &Body, chromedp.ByQuery),
	); err != nil {
		log.Println(err)
	}

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(Body))
	if err != nil {
		log.Println(err)
	}

	pagbar := doc.Find("ol.pagination.actions").Text()
	var output string

	input := pagbar

	for _, char := range input {
		if unicode.IsSpace(char) {
			output += ","
		} else if unicode.IsNumber(char) && char != '…' {
			output += string(char)
		}
	}

	// Remove leading and trailing commas
	output = strings.Trim(output, ",")

	// Split the cleaned output string into a slice
	numbersSlice := strings.Split(output, ",")

	// Initialize the maximum value to the minimum possible integer value
	max := int(^uint(0)>>1) * -1

	// Iterate through the slice, convert each string to an integer, and update the maximum value if necessary
	for _, strNum := range numbersSlice {
		num, err := strconv.Atoi(strNum)
		if err == nil && num > max {
			max = num
		}
	}

	for i := 1; i <= max; i++ {

		log.Printf("Page %d out of %d\n", i, max)

		HistoryPage := GetHistoryPage(Name, i, ctx)

		History = append(History, HistoryPage...)

	}

	return History

}

func GetHistoryPage(Name string, PageNB int, ctx context.Context) (History []models.History) {

	// var Body string

	url := fmt.Sprintf("https://archiveofourown.org/users/%s/readings?page=%d", Name, PageNB)
	if err := chromedp.Run(ctx,
		chromedp.Sleep(1*time.Second),

		chromedp.Navigate(url),

		// chromedp.WaitVisible("body", chromedp.ByQuery),

		chromedp.ActionFunc(func(ctx context.Context) error {

			var html string
			chromedp.OuterHTML(`document.querySelector("#main > ol.reading.work.index.group")`, &html, chromedp.ByJSPath).Do(ctx)

			doc, err := goquery.NewDocumentFromReader(strings.NewReader(html))
			if err != nil {
				return err
			}

			// var w []string

			doc.Find("ol.reading.work.index.group > li").Each(func(i int, s *goquery.Selection) {
				var history models.History
				// log.Println(s.Html())

				var title string
				var authors []string

				// log.Println(s.Find("h4.heading > a, h4.heading").Html())
				// log.Println(s.Find("h4.heading > a, h4.heading > a[rel=author]").Html())

				s.Find("h4.heading > a").Each(func(i int, el *goquery.Selection) {

					if rel, exists := el.Attr("rel"); exists && rel == "author" {
						author := el.Text()
						authors = append(authors, author)
					} else {
						title = el.Text()
						if val, ok := el.Attr("href"); ok {
							// log.Println(ok, val)
							if strings.Contains(val, "works") {
								history.WorkID, _ = utils.GetWorkID(val)
							}

							history.URL = fmt.Sprintf("https://archiveofourown.org/works/%s", history.WorkID)

						}

					}
				})
				// log.Println(strings.Join(authors, " "))
				history.Title = title
				history.Author = authors
				// log.Println(history.URL, history.WorkID)

				// history.Fandom = append(history.Fandom, s.Find("h5.fandoms.heading > a").Text())
				// history.Fandom

				s.Find("h5.fandoms.heading > a.tag").Each(func(i int, el *goquery.Selection) {
					history.Fandom = append(history.Fandom, strings.TrimSpace(el.Text()))

				})

				// log.Println(strings.Join(history.Fandom, "; "))

				// p.datetime

				Updated := s.Find("div.header.module > p.datetime").Text()
				if len(Updated) != 0 {
					history.Updated, err = time.Parse("02 Jan 2006", Updated)
					if err != nil {
						log.Println("error with time", err)
						// log.Println()
					}
				}

				//TODO fix date visited

				// document.querySelector("#work_29691903 > div.user.module.group > h4")

				// s.Find("div.user.module.group").Text()

				DateVisited := s.Find("div.user.module.group").Text()
				if len(DateVisited) != 0 {
					DateVisited = strings.ReplaceAll(DateVisited, "\n", "")
					DateVisited := strings.TrimSpace(DateVisited)

					re := regexp.MustCompile(`(\d{1,2}\s\w{3}\s\d{4})`)
					matches := re.FindStringSubmatch(DateVisited)
					if len(matches) > 1 {
						dateStr := matches[1]
						date, err := time.Parse("02 Jan 2006", dateStr)
						if err != nil {
							fmt.Println("Error parsing date:", err)
							return
						}
						// fmt.Println("Extracted date:", date)
						history.DateVisited = date
						// Remove extracted text from string and save as new string
						DateVisited = re.ReplaceAllString(DateVisited, "")
					} else {
						fmt.Println("No date found in string")
					}

					regexUpdated := regexp.MustCompile(`\((?P<update>[^)]+)\)`)
					regexVisited := regexp.MustCompile(`Visited\s+(?P<visited>.+)`)

					matchedUpdated := utils.FindNamedMatches(regexUpdated, DateVisited)
					matchedVisited := utils.FindNamedMatches(regexVisited, DateVisited)
					history.VisitVersion = matchedUpdated["update"]

					if strings.Contains(matchedVisited["visited"], "once") {
						history.VisitNumber = 1
					} else {
						r := regexp.MustCompile(`\b(\d+)\b`) // add word boundary to match whole numbers
						match := r.FindStringSubmatch(matchedVisited["visited"])
						if len(match) == 2 { // check length of match slice
							num, err := strconv.Atoi(match[1])
							if err != nil {
								fmt.Println("Error converting number:", err)
								history.VisitNumber = 0 // use default value if conversion fails
							} else {
								history.VisitNumber = num
							}
						} else {
							fmt.Println("No number found")
							history.VisitNumber = 0 // use default value if no number found
						}
					}

				}

				s.Find("blockquote.userstuff.summary").Each(func(i int, el *goquery.Selection) {
					el.Find("p").Each(func(i int, ez *goquery.Selection) {
						history.Summary = append(history.Summary, ez.Text())
					})
				})

				s.Find("li.relationships > a.tag").Each(func(i int, el *goquery.Selection) {
					history.Relationship = append(history.Relationship, el.Text())

				})

				s.Find("ul.required-tags > li").Each(func(_ int, el *goquery.Selection) {
					history.RequiredTags = append(history.RequiredTags, el.Find("a").Text())
				})

				// tags commas

				s.Find("ul.tags.commas > li").Each(func(_ int, el *goquery.Selection) {
					history.AlternativeTags = append(history.AlternativeTags, el.Find("a").Text())
				})

				s.Find("dl.stats > dd").Each(func(i int, el *goquery.Selection) {
					// log.Println(el.Text())
					n := el.Get(0) // Retrieves the internal *html.Node
					for _, a := range n.Attr {
						// log.Println(a.Val)
						switch a.Val {
						case "language":
							history.Language = el.Text()
						case "words":
							history.Words = el.Text()
						case "chapters":
							history.NBChapters = el.Text()
						case "comments":
							intVar, err := strconv.Atoi(el.Text())
							if err != nil {
								log.Println(err)
							}
							history.Comments = intVar
						case "kudos":
							intVar, err := strconv.Atoi(el.Text())
							if err != nil {
								log.Println(err)
							}
							history.Kudos = intVar
						case "bookmarks":
							intVar, err := strconv.Atoi(el.Text())
							if err != nil {
								log.Println(err)
							}
							history.Bookmarks = intVar
						case "hits":
							hits := strings.ReplaceAll(el.Text(), ",", "")
							intVar, err := strconv.Atoi(hits)
							if err != nil {
								log.Println(err)
							}
							history.Hits = intVar
						}
					}

				})

				History = append(History, history)

			})

			return nil
		}),
	); err != nil {
		log.Println(err)
	}

	return History

}
