package utils

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/cdproto/storage"
	"github.com/chromedp/chromedp"
)

func GetWorkID(val string) (WorkID, ChapterID string) {

	log.Println(val)

	re := regexp.MustCompile(`archiveofourown\.org\/works\/(?P<work_id>[0-9]+)(?:\/chapters\/(?P<chapter_number>[0-9]+))?`)

	match := re.FindStringSubmatch(val)
	log.Println(match)
	if len(match) > 0 {
		for i, name := range re.SubexpNames() {
			if i != 0 && name != "" {
				// fmt.Printf("%s: %s\n", name, match[i])
				if name == "work_id" {
					WorkID = match[i]
				} else if name == "chapter_number" {
					ChapterID = match[i]
				}
			}
		}
	}

	log.Println(WorkID, ChapterID)
	return WorkID, ChapterID
	// return "", ""

}

func FindUrl(urls []string) (max int) {
	var err error
	var nb []int
	var tnb int
	for _, url := range urls {
		slash := strings.HasSuffix(url, "/")
		if !slash {

			splitPath := strings.Split(url, "/")
			a := splitPath[len(splitPath)-1]
			re := regexp.MustCompile(`\?page=[0-9]+`)
			z := re.FindAllString(a, -1)
			z = strings.Split(z[0], "=")
			tnb, err = strconv.Atoi(z[1])
			nb = append(nb, tnb)
			if err != nil {
				log.Println(err)
			}
		}
	}
	if len(nb) == 0 {
		return 0
	} else {
		return MaxIntSlice(nb)
	}
}

func MaxIntSlice(v []int) int {
	sort.Ints(v)
	return v[len(v)-1]
}

// readLines reads a whole file into memory
// and returns a slice of its lines.
func ReadLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

// writeLines writes the lines to the given file.
func WriteLines(lines []string, path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	for _, line := range lines {
		fmt.Fprintln(w, line)
	}
	return w.Flush()
}

func Timer(s int) {
	// run infinite loop
	for {
		// check if end condition is met
		if s <= 0 {
			break
		} else {
			log.Printf("waiting for %d", s)
			time.Sleep(1 * time.Second) // wait 1 sec
			s--                         // reduce time
		}
	}
}

func WriteJson(filename string, a interface{}) {
	b, err := json.MarshalIndent(a, "", "  ")
	if err != nil {
		log.Println(err)
	}

	ioutil.WriteFile(filename, b, os.ModePerm)
}

func Str2int(str string) (intvar int, err error) {
	intVar, err := strconv.Atoi(str)
	if err != nil {
		// log.Println(err)
		return 0, err
	}
	return intVar, nil
}

func DropOf(val string) string {

	splitStr := strings.Split(val, "of")
	// log.Println(splitStr, len(splitStr))
	// splitStr
	return splitStr[0]
}

func DropHyphen(val string) (Out []int) {
	splitStr := strings.Split(val, "-")
	// log.Println(splitStr, len(splitStr))
	// splitStr

	for _, str := range splitStr {
		nb1, err := Str2int(strings.TrimSpace(str))
		if err != nil {
			log.Println(err)
		}

		Out = append(Out, nb1)
	}

	return Out
}

func GetWorkPerPage(str string) {
	StringRange := DropOf(str)
	Range := DropHyphen(StringRange)

	lo, hi := Range[0], Range[1]
	s := make([]int, hi-lo+1)
	for i := range s {
		s[i] = i + lo
	}

	log.Println(len(s))
}

// func RemoveDuplicateID(sample []models.ID) []models.ID {
// 	var unique []models.ID

// 	for _, v := range sample {
// 		skip := false
// 		for _, u := range unique {
// 			if v.WorkID == u.WorkID {
// 				skip = true
// 				break
// 			}
// 		}
// 		if !skip {
// 			unique = append(unique, v)
// 		}
// 	}
// 	return unique

// }

func RemoveDuplicateAuthor(sample []string) []string {
	var unique []string

	for _, v := range sample {
		skip := false
		for _, u := range unique {
			if v == u {
				skip = true
				break
			}
		}
		if !skip {
			unique = append(unique, v)
		}
	}
	return unique

}

// SliceFind search string in slice of string
func SliceFind(slice []string, val string) (int, bool) {
	for i, item := range slice {

		if item == val {
			return i, true
		}
	}
	return -1, false
}

// SliceIndex Get position of element in slice
func SliceIndex(limit int, predicate func(i int) bool) int {
	for i := 0; i < limit; i++ {
		if predicate(i) {
			return i
		}
	}
	return -1
}

// DeleteEmpty ...
func DeleteEmpty(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}

func RemoveDupStr(sample []string) []string {
	var unique []string
	for _, v := range sample {
		skip := false
		for _, u := range unique {
			if v == u {
				skip = true
				break
			}
		}
		if !skip {
			unique = append(unique, v)
		}
	}
	return unique
}

// func GetFanficID(fanfic string) (idWork models.ID) {

// 	var wID, wChap string

// 	splitPath := strings.Split(fanfic, "/")
// 	works := SliceIndex(len(splitPath), func(i int) bool { return splitPath[i] == "works" })
// 	chapters := SliceIndex(len(splitPath), func(i int) bool { return splitPath[i] == "chapters" })
// 	// log.Println(works, chapters)
// 	if len(splitPath) == 5 {
// 		//log.Println("Oneshot")
// 		//log.Println(splitPath[works+1])
// 		wID = splitPath[works+1]
// 		//log.Println(len(wID), wID)
// 		idWork.WorkID = wID
// 		idWork.ChapterID = ""
// 	} else if len(splitPath) == 7 {
// 		//print("MultiChapters")
// 		// log.Println(splitPath[works+1], splitPath[chapters+1])
// 		wID = splitPath[works+1]
// 		wChap = splitPath[chapters+1]
// 		idWork.WorkID = wID
// 		idWork.ChapterID = wChap
// 		//log.Println(wID)

// 	}

// 	return idWork
// }

func GetFanficURL(fanfic string) (fanfics string) {
	//var chaps []string
	// var wID string
	// var wChap string
	//	var a int

	//archive := regexp.MustCompile(`https?:\/\/archiveofourown.org\/works\/[0-9]+`)
	archive := regexp.MustCompile(`https?://archiveofourown.org/works/[0-9]+(?:/chapters/[0-9]+)?`)
	fanfics = archive.FindString(fanfic)
	// return idsWork
	return fanfics
}

func SetCookie(TimeInt int64, name, value, domain, path string, httpOnly, secure bool) chromedp.Action {
	return chromedp.ActionFunc(func(ctx context.Context) error {
		Time := time.Unix(0, TimeInt)

		expr := cdp.TimeSinceEpoch(Time)
		err := network.SetCookie(name, value).
			WithExpires(&expr).
			WithDomain(domain).
			WithPath(path).
			WithHTTPOnly(httpOnly).
			WithSecure(secure).
			Do(ctx)
		if err != nil {
			return err
		}
		return nil
	})
}

func ShowCookies() chromedp.Action {
	return chromedp.ActionFunc(func(ctx context.Context) error {
		cookies, err := storage.GetCookies().Do(ctx)
		if err != nil {
			return err
		}

		for i, cookie := range cookies {
			log.Printf("chrome cookie %d: %+v", i, cookie)
		}
		return nil
	})
}

func RandSleep(min, max int) {
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(max-min) + min
	log.Printf("Sleeping %d seconds...", n)
	for i := n; i > 0; i-- {
		fmt.Printf("\r%d...", i)
		time.Sleep(time.Second)
	}
	fmt.Println("\rDone")
}

func CheckJSONFile(filepath string) error {
	// Check if the file exists
	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		return fmt.Errorf("JSON file '%s' does not exist", filepath)
	}

	// Open the JSON file
	file, err := os.Open(filepath)
	if err != nil {
		return fmt.Errorf("error opening JSON file '%s': %v", filepath, err)
	}
	defer file.Close()

	// Read the contents of the file into a byte slice
	data, err := ioutil.ReadAll(file)
	if err != nil {
		return fmt.Errorf("error reading JSON file '%s': %v", filepath, err)
	}

	// Unmarshal the JSON data into an interface{}
	var v interface{}
	err = json.Unmarshal(data, &v)
	if err != nil {
		return fmt.Errorf("error unmarshaling JSON file '%s': %v", filepath, err)
	}

	// Check if the value is nil or empty
	if v == nil {
		return fmt.Errorf("JSON file '%s' is null", filepath)
	} else if len(data) == 0 {
		return fmt.Errorf("JSON file '%s' is empty", filepath)
	}

	return nil
}

func FindNamedMatches(regex *regexp.Regexp, str string) map[string]string {
	match := regex.FindStringSubmatch(str)
	// log.Println(match)
	results := map[string]string{}
	for i, name := range match {
		results[regex.SubexpNames()[i]] = name
	}
	return results
}
