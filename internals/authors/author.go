package author

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/dom"
	"github.com/chromedp/cdproto/runtime"
	"github.com/chromedp/chromedp"
	"gitlab.com/capoverflow/ao3apicdp/internals/models"
	"gitlab.com/capoverflow/ao3apicdp/internals/utils"
)

/*
url
https://archiveofourown.org/users/%s/
https://archiveofourown.org/users/%s/profile
https://archiveofourown.org/users/%s/works
https://archiveofourown.org/users/%s/series
https://archiveofourown.org/users/%s/bookmarks
https://archiveofourown.org/users/%s/gifts
*/

func GetAuthor(Params models.AuthorParams, ctx context.Context) (author models.Author, err error) {
	author.Author = Params.Author
	log.Println(Params)

	var data string

	// do profile

	var nouser string

	url := fmt.Sprintf("https://archiveofourown.org/users/%s/profile", Params.Author)

	log.Println(url)

	if err := chromedp.Run(ctx,

		chromedp.Navigate(url),

		chromedp.ActionFunc(
			func(ctx context.Context) error {
				_, exp, err := runtime.Evaluate(`window.localStorage.setItem('accepted_tos', '20180523')`).Do(ctx)
				if err != nil {
					return err
				}
				if exp != nil {
					return exp
				}
				return nil
			}),

		chromedp.Navigate(url),

		// chromedp.Text(`document.querySelector("#main > div.flash.error")`, &nouser, chromedp.ByJSPath),
		// chromedp.

		chromedp.OuterHTML("html", &data, chromedp.ByQuery),

		// chromedp.WaitVisible(``),

	); err != nil {
		log.Println(err)
	}

	// log.Println(nouser)

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(data))
	if err != nil {
		log.Println(err)
	}

	nouser = doc.Find("div.flash.error").Text()

	if len(nouser) == 0 {
		doc.Find("dd.pseuds > a").Each(func(i int, s *goquery.Selection) {
			// log.Println(s.Text())
			if val, ok := s.Attr("href"); !ok {
				log.Println("no link")
			} else {
				// log.Println(val)
				// log.Println(s.Text())
				author.Profile.Pseuds = append(author.Profile.Pseuds, val)
			}

		})

		author.Profile.JoinDate = doc.Find("dd:nth-child(4)").Text()
		author.Profile.Bio = doc.Find("div.bio.module > blockquote").Text()

		author.Works = GetWork(ctx, Params)

		if Params.Bookmarks {
			author.Bookmarks = GetBookmarks(ctx, Params)
		}

		return author, nil
	} else {
		return models.Author{
			Author: Params.Author,
		}, errors.New("no user found please skip")
	}
}

func GetBookmarks(ctx context.Context, Params models.AuthorParams) (Bookmarks []models.Bookmark) {
	log.Println("Bookmarks")
	Bookmarks, PageNB := GetBookmarksFirstPage(ctx, Params)

	// fmt.Println(Bookmarks)

	// do not forget to uncomments for enabling multipage
	if PageNB > 0 {
		for i := 2; i <= PageNB; i++ {

			if Params.WorkPageSpleep != 0 {
				Bookmarks = append(Bookmarks, GetBookmarksPage(ctx, Params, i)...)
				utils.RandSleep(1, Params.WorkPageSpleep)

			} else {
				Bookmarks = append(Bookmarks, GetBookmarksPage(ctx, Params, i)...)
			}

			// Works = append(Works)
		}
	}

	return Bookmarks
}

func GetBookmarksFirstPage(ctx context.Context, Params models.AuthorParams) (Bookmarks []models.Bookmark, PageNB int) {

	url := fmt.Sprintf("https://archiveofourown.org/users/%s/bookmarks?page=1", Params.Author)

	var nodes []*cdp.Node
	var data string
	var err error
	var pageinfo string

	// log.Println(url)

	if err := chromedp.Run(ctx,

		chromedp.Navigate(url),

		chromedp.Text(`document.querySelector("#main > h2")`, &pageinfo, chromedp.ByJSPath),

		chromedp.OuterHTML("html", &data, chromedp.ByQuery),

		// chromedp.WaitVisible("TMP", chromedp.ByID),
		chromedp.Nodes(`document.querySelector("#main > ol:nth-child(5)")`, &nodes, chromedp.ByJSPath),

		chromedp.Sleep(1*time.Second),

		chromedp.ActionFunc(func(ctx context.Context) error {

			for _, node := range nodes {
				dom.RequestChildNodes(node.NodeID).WithDepth(-1).Do(ctx)
			}

			return err

		}),
	); err != nil {
		log.Println(err)
	}

	Bookmarks = GetBoomarksDom(data)

	node := nodes[0]

	if err := chromedp.Run(ctx,
		chromedp.OuterHTML([]cdp.NodeID{node.NodeID}, &data, chromedp.ByNodeID),
	); err != nil {
		log.Printf("Run err : %v\n", err)
	}

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(data))
	if err != nil {
		log.Println(err)
	}

	var pages []string

	doc.Find("ol.pagination.actions > li").Each(func(i int, s *goquery.Selection) {
		if val, ok := s.Find("a").Attr("href"); !ok {
			// log.Println("no link ")
		} else {
			// log.Println(val)
			pages = append(pages, val)
			// log.Println(s.Text())
		}

	})

	return Bookmarks, utils.FindUrl(pages)

}

func GetBookmarksPage(ctx context.Context, Params models.AuthorParams, PageNB int) (Bookmarks []models.Bookmark) {

	url := fmt.Sprintf("https://archiveofourown.org/users/%s/bookmarks?page=%d", Params.Author, PageNB)

	var pageinfo string

	var data string

	// log.Println(url)

	if err := chromedp.Run(ctx,

		chromedp.Navigate(url),

		chromedp.Text(`document.querySelector("#main > h2")`, &pageinfo, chromedp.ByJSPath),

		chromedp.OuterHTML("html", &data, chromedp.ByQuery),

		chromedp.Sleep(1*time.Second),
	); err != nil {
		log.Println(err)
	}
	return GetBoomarksDom(data)

}

func GetBoomarksDom(data string) (Bookmarks []models.Bookmark) {

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(data))
	if err != nil {
		log.Println(err)
	}

	doc.Find("ol.bookmark.index.group > li").Each(func(i int, s *goquery.Selection) {
		var bookmark models.Bookmark
		// log.Println(s.Html()

		var title string
		var authors []string

		s.Find("h4.heading > a, h4.heading > a[rel=author]").Each(func(i int, el *goquery.Selection) {
			if rel, exists := el.Attr("rel"); exists && rel == "author" {
				authors = append(authors, el.Text())
			} else {
				title = el.Text()
			}
		})

		bookmark.Title = title
		bookmark.Author = authors

		bookmark.Fandom = append(bookmark.Fandom, s.Find("h5.fandoms.heading > a").Text())

		// p.datetime

		Updated := s.Find("div.header.module > p.datetime").Text()
		if len(Updated) != 0 {
			bookmark.Updated, err = time.Parse("02 Jan 2006", Updated)
			if err != nil {
				log.Println("error with time", err)
				// log.Println()
			}
		}

		DateBookmarked := s.Find("div.user.module.group > p.datetime").Text()
		if len(DateBookmarked) != 0 {
			bookmark.DateBookmarked, err = time.Parse("02 Jan 2006", DateBookmarked)
			if err != nil {
				log.Println("error with time", err)
				// log.Println()
			}
		}

		s.Find("blockquote.userstuff.summary").Each(func(i int, el *goquery.Selection) {
			el.Find("p").Each(func(i int, ez *goquery.Selection) {
				bookmark.Summary = append(bookmark.Summary, ez.Text())
			})
		})

		s.Find("li.relationships > a.tag").Each(func(i int, el *goquery.Selection) {
			bookmark.Relationship = append(bookmark.Relationship, el.Text())

		})

		s.Find("ul.required-tags > li").Each(func(_ int, el *goquery.Selection) {
			bookmark.RequiredTags = append(bookmark.RequiredTags, el.Find("a").Text())
		})

		// tags commas

		s.Find("ul.tags.commas > li").Each(func(_ int, el *goquery.Selection) {
			bookmark.AlternativeTags = append(bookmark.AlternativeTags, el.Find("a").Text())
		})

		s.Find("dl.stats > dd").Each(func(i int, el *goquery.Selection) {
			// log.Println(el.Text())
			n := el.Get(0) // Retrieves the internal *html.Node
			for _, a := range n.Attr {
				// log.Println(a.Val)
				switch a.Val {
				case "language":
					bookmark.Language = el.Text()
				case "words":
					bookmark.Words = el.Text()
				case "chapters":
					bookmark.NBChapters = el.Text()
				case "comments":
					intVar, err := strconv.Atoi(el.Text())
					if err != nil {
						log.Println(err)
					}
					bookmark.Comments = intVar
				case "kudos":
					intVar, err := strconv.Atoi(el.Text())
					if err != nil {
						log.Println(err)
					}
					bookmark.Kudos = intVar
				case "bookmarks":
					intVar, err := strconv.Atoi(el.Text())
					if err != nil {
						log.Println(err)
					}
					bookmark.Bookmarks = intVar
				case "hits":
					intVar, err := strconv.Atoi(el.Text())
					if err != nil {
						log.Println(err)
					}
					bookmark.Hits = intVar
				}
			}

		})

		Bookmarks = append(Bookmarks, bookmark)

	})

	return Bookmarks
}

func GetWork(ctx context.Context, Params models.AuthorParams) (Works []models.Work) {
	log.Println("Works")

	Works, PageNB := GetWorkFirstPage(ctx, Params)

	// do not forget to uncomments for enabling multipage
	if PageNB > 0 {
		log.Println("PageNB > 0")
		for i := 2; i <= PageNB; i++ {

			if Params.WorkPageSpleep != 0 {
				Works = append(Works, GetWorkPage(ctx, Params, i)...)
				utils.RandSleep(1, Params.WorkPageSpleep)

			} else {
				Works = append(Works, GetWorkPage(ctx, Params, i)...)
			}

			// Works = append(Works)
		}
	}

	return Works
}

func GetWorkFirstPage(ctx context.Context, Params models.AuthorParams) (Works []models.Work, PageNB int) {

	url := fmt.Sprintf("https://archiveofourown.org/users/%s/works?page=1", Params.Author)

	var nodes []*cdp.Node
	var data string
	var err error
	var pageinfo string

	// log.Println(url)

	if err := chromedp.Run(ctx,

		chromedp.Navigate(url),

		chromedp.Text(`document.querySelector("#main > h2")`, &pageinfo, chromedp.ByJSPath),

		chromedp.OuterHTML("html", &data, chromedp.ByQuery),

		// chromedp.WaitVisible("TMP", chromedp.ByID),
		chromedp.Nodes(`document.querySelector("#main > ol:nth-child(5)")`, &nodes, chromedp.ByJSPath),

		chromedp.Sleep(1*time.Second),

		chromedp.ActionFunc(func(ctx context.Context) error {

			for _, node := range nodes {
				dom.RequestChildNodes(node.NodeID).WithDepth(-1).Do(ctx)
			}

			return err

		}),
	); err != nil {
		log.Println(err)
	}

	Works = GetWorkDom(data)

	node := nodes[0]

	if err := chromedp.Run(ctx,
		chromedp.OuterHTML([]cdp.NodeID{node.NodeID}, &data, chromedp.ByNodeID),
	); err != nil {
		log.Printf("Run err : %v\n", err)
	}

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(data))
	if err != nil {
		log.Println(err)
	}

	var pages []string

	doc.Find("ol.pagination.actions > li").Each(func(i int, s *goquery.Selection) {
		if val, ok := s.Find("a").Attr("href"); !ok {
			// log.Println("no link ")
		} else {
			log.Println(val)
			pages = append(pages, val)
			// log.Println(s.Text())
		}

	})

	return Works, utils.FindUrl(pages)

}

func GetWorkPage(ctx context.Context, Params models.AuthorParams, PageNB int) (Works []models.Work) {

	url := fmt.Sprintf("https://archiveofourown.org/users/%s/works?page=%d", Params.Author, PageNB)

	var pageinfo string

	var data string

	// log.Println(url)

	if err := chromedp.Run(ctx,

		chromedp.Navigate(url),

		chromedp.Text(`document.querySelector("#main > h2")`, &pageinfo, chromedp.ByJSPath),

		chromedp.OuterHTML("html", &data, chromedp.ByQuery),

		chromedp.Sleep(1*time.Second),
	); err != nil {
		log.Println(err)
	}
	return GetWorkDom(data)

}

func GetWorkDom(data string) (works []models.Work) {

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(data))
	if err != nil {
		log.Println(err)
	}

	var w []string

	doc.Find("ol.work.index.group > li").Each(func(i int, s *goquery.Selection) {
		var work models.Work
		// log.Println(s.Html())

		s.Find("h4.heading > a").Each(func(i int, el *goquery.Selection) {
			if val, ok := el.Attr("href"); !ok {
			} else {

				if strings.Contains(val, "works") {
					work.Title = el.Text()
					w = append(w, el.Text())
					// log.Println(val)
					// work.WorkID, _ = utils.GetWorkID(val)
					valSplit := strings.Split(val, "/")
					// log.Println(valSplit, valSplit[len(valSplit)-1])
					work.WorkID = valSplit[len(valSplit)-1]
					work.URL = fmt.Sprintf("https://archiveofourown.org/works/%s", work.WorkID)

				} else if strings.Contains(val, "users") {
					work.Author = append(work.Author, el.Text())
					// work.Author = RemoveDupStr(work.Author)
				}

			}

		})

		// p.datetime

		work.Fandom = append(work.Fandom, s.Find("h5.fandoms.heading > a").Text())

		Updated := s.Find("p.datetime").Text()
		// log.Println(Updated)
		if len(Updated) != 0 {
			work.Updated, err = time.Parse("02 Jan 2006", Updated)
			if err != nil {
				log.Println("error with time", err)
			}
		}

		s.Find("blockquote.userstuff.summary").Each(func(i int, el *goquery.Selection) {
			el.Find("p").Each(func(i int, ez *goquery.Selection) {
				work.Summary = append(work.Summary, ez.Text())
			})
		})

		s.Find("li.relationships > a.tag").Each(func(i int, el *goquery.Selection) {
			work.Relationship = append(work.Relationship, el.Text())

		})

		// document.querySelector("#work_41270814 > div > ul.required-tags")

		// s.Find("ul.required-tags").Html()

		s.Find("ul.required-tags > li").Each(func(_ int, el *goquery.Selection) {
			work.RequiredTags = append(work.RequiredTags, el.Find("a").Text())
		})

		// tags commas

		s.Find("ul.tags.commas > li").Each(func(_ int, el *goquery.Selection) {
			work.AlternativeTags = append(work.AlternativeTags, el.Find("a").Text())
		})

		// log.Println(s.Find("dl.stats").Text())

		s.Find("dl.stats > dd").Each(func(i int, el *goquery.Selection) {
			// log.Println(el.Text())
			n := el.Get(0) // Retrieves the internal *html.Node
			for _, a := range n.Attr {
				// log.Println(a.Val)
				switch a.Val {
				case "language":
					work.Language = el.Text()
				case "words":
					work.Words = el.Text()
				case "chapters":
					work.NBChapters = el.Text()
				case "comments":

					work.Comments = el.Text()
				case "kudos":

					work.Kudos = el.Text()
				case "bookmarks":

					work.Bookmarks = el.Text()
				case "hits":

					work.Hits = el.Text()
				}
			}

		})

		works = append(works, work)

	})

	return works
}
