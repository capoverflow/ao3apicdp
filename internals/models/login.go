package models

type Login struct {
	Scheme     string
	Addr       string
	Login      string
	Username   string
	Password   string
	RememberMe bool
}
